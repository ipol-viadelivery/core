<?php


namespace Ipol\Viadelivery\Core\Entity\Packing;


/**
 * Interface DimensionsMerger
 * @package Ipol\Viadelivery\Core
 * @subpackage Packing
 */
interface DimensionsMerger
{
    public static function getSumDimensions($arGabs);

}